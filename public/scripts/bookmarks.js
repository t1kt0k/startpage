const bookmarksMap = new Map([
    ['Personal', [
        {'ESV Bible': 'https://esv.org'},
        {'TableTalk Magazine': 'https://www.ligonier.org/'},
        {'Logos': 'https://app.logos.com/'}
        
    ]],

    ['WoW', [
        {'WoWHead': 'https://wowhead.com'},
        {'Ask Mr Robot': 'https://www.askmrrobot.com/'}
        
    ]],
['Music', [
        {'Apple Music': 'https://music.apple.com'},
        {'Youtube Music': 'https://music.youtube.com'},
        {'Spotify': 'https://spotify.com'}
        
        
    ]],

    ['Social', [
        {'Facebook': 'https://facebook.com'},
        {'X Pro': 'https://pro.twitter.com'},
        {'X': 'https://twitter.com'},
        {'LinkIn': 'https://linkedin.com'}
        
             
    ]],
    ['Coding', [
        {'Free Code Camp': 'https://freecodecamp.org'},
        {'Gitlab': 'https://gitlab.com/'},
        {'Udemy': 'https://udemy.com'},
        {'Codepen': 'https://codepen.io'}
        
    ]],
    ['CyberSec', [
        {'TryHackMe': 'https://tryhackme.com'},
        {'HackTheBox': 'https://hackthebox.com'},
        {'Hackersploit Academy': 'https://hackersploit-academy.thinkific.com/'},
        {'Hackersploit Blog' : 'https://hackersploit.org/'}

    ]],
    ['Mail', [
        {'Protonmail': 'https://mail.proton.me/u/0/inbox'},
        {'Gmail': 'https://gmail.com'}
      
    ]],
    ['Video', [
        {'Plex': 'https://app.plex.tv'},
        {'Netflix': 'https://netflix.com'},
        {'Pureflix': 'https://pureflix.com'},
        {'Odysee': 'https://odysee.com'},
        {'Youtube': 'https://youtube.com'},
        {'Rumble': 'https://rumble.com'}
       
    ]],
     ['News', [
        {'1st Headlines': 'https://www.1stheadlines.com/'},
        {'Ground News': 'https://ground.news/'}
       
      
       
    ]],

]);

function sortMap() {
    let isEven = false;
    for (let [, bookmarks] of bookmarksMap) {
        for (let i = 0; i < bookmarks.length; i++) {
            for (let j = i + 1; j < bookmarks.length; j++) {
                const len = obj => Object.keys(obj)[0].length;
                const isAscending = len(bookmarks[i]) < len(bookmarks[j]);
                if (isEven && isAscending || !isEven && !isAscending)
                    [bookmarks[i], bookmarks[j]] = [bookmarks[j], bookmarks[i]];
            }
        }
        isEven = !isEven;
    }
}

function populateLinks() {
    const nav = document.createElement('nav');
    nav.id = 'links';
    for (let [ctg, bookmarks] of bookmarksMap) {
        const list = document.createElement('ul');
        const header = document.createElement('li');
        header.className = 'accent';
        header.innerHTML = ctg;
        list.appendChild(header);
        for (let bookmark of bookmarks) {
            const el = document.createElement('li');
            const anchor = document.createElement('a');
            anchor.href = Object.values(bookmark)[0];
            anchor.innerHTML = Object.keys(bookmark)[0];
            el.appendChild(anchor);
            list.appendChild(el);
        }
        nav.appendChild(list);
    }
    document.getElementById('img').insertAdjacentElement('afterend', nav);
}

sortMap();
populateLinks();
